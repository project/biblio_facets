
README file for the Biblio Facets Drupal module.


Description
***********

Biblio Facets integrates with Faceted Search to allow users to browse Biblio
types and fields as facets.


Requirements
************

- Drupal 5.x (http://drupal.org/project/drupal).

- Faceted Search (http://drupal.org/project/faceted_search).

- Biblio (http://drupal.org/project/biblio).

- Biblio Normalize (http://drupal.org/project/biblio_normalize). Actually, this
  module is only needed if you wish to expose Biblio's multiple-values fields
  (for example, the Authors and the Keywords fields), but most people using
  Biblio Facets will want this.


Installation
************

1. Extract the 'biblio_facets module directory into your Drupal modules
   directory.

2. Go to the Administer > Site building > Modules page. If you wish to expose
   Biblio types as facets, enable the Biblio Type Facet module. If you wish to
   expose Biblio fields as facets, enable the Biblio Facets modules.

3. Go to the Administer > Site configuration > Faceted Search page, choose to
   edit the faceted search environment that shall expose Biblio facets. In the
   environment editing form, check each facet you wish to expose.

   CAUTION: If you want to expose a multiple-values field as a facet, you should
   enable its normalization first through Biblio Normalize.

   CAUTION: Biblio Facets hands you over the responsibility of deciding what
   field is appropriate for use as a facet. Full-text fields should never be
   used as facets, since their values are too long to be usable in the Guided
   search, and might exceed the maximum URL length in browsers or servers.

4. If you wish to fully replace Biblio's search and filtering capabilities with
   Biblio Facets, you'll want to override Biblio's theming functions to replace
   the links that are normally provided.


Support
*******

For support requests, bug reports, and feature requests, please use the
project's issue queue on http://drupal.org/project/issues/biblio_facets.

Please DO NOT send bug reports through e-mail or personal contact forms, use the
aforementioned issue queue instead.


Credits
*******

* Sponsored by Laboratoire NT2 (http://www.labo-nt2.uqam.ca).

